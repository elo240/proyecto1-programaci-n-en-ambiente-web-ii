﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto1.Class;
using Proyecto1.Models;

namespace Proyecto1.Controllers
{
    public class HomeController : Controller
    {
        private CurriculumDb db = new CurriculumDb();
        //
        // GET: /Home/


        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult Recommend()
        {
            List<Recommends> recommends = db.recommends.ToList();
            recommends.Reverse();
            ViewBag.recommends = recommends;
            return View();
        }
        [HttpPost]
        public ActionResult SendEmail(string toAddress, string subject, string body)
        {
            if (!(toAddress.Equals("") || subject.Equals("") || body.Equals("")))
            {
                Email email = new Email();
                email.Send(toAddress, subject, body);
                return RedirectToAction("Contact");
            }
            return View("Contact");
        }
        [HttpPost]
        public ActionResult CreateRecommend(string comment="", string name="", string phone="", string email="")
        {
            if (!(comment.Equals("") || name.Equals("") || email.Equals("")))
            {

                DateTime Hoy = DateTime.Today;
                Recommends newRecomment = new Recommends();
                newRecomment.Comment = comment;
                newRecomment.Name = name;
                newRecomment.Phone = phone;
                newRecomment.Email = email;
                newRecomment.Date = Hoy.ToString("dd-MM-yyyy");
                if (ModelState.IsValid)
                {
                    db.recommends.Add(newRecomment);
                    db.SaveChanges();
                    return RedirectToAction("Recommend");
                }
            }
            return View("Recommend");
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
