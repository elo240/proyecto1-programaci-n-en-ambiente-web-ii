﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Proyecto1.Models
{
    public class CurriculumDb : DbContext
    {
        public CurriculumDb()
            : base("Curriculum1")
        {
            
        }

        public DbSet<Images> images { get; set; }
        public DbSet<Recommends> recommends { get; set; }
    }
}