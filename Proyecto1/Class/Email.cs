﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace Proyecto1.Class
{
    public class Email
    {
        public void Send(string toAddress, string subject, string body)
        {
            string Body = "Contacto de: " + toAddress + "<br> Con el siguiente cuerpo:<br>" + body;
            var mailMessage = new MailMessage();
            mailMessage.To.Add(System.Configuration.ConfigurationManager.AppSettings.Get("ToEmail"));
            mailMessage.Subject = subject;
            mailMessage.Body = Body;
            mailMessage.IsBodyHtml = true;

            var smtpClient = new SmtpClient { EnableSsl = true };
            smtpClient.Send(mailMessage);
        }
    }
}